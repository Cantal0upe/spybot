import time
import calendar

class Timer:
    
    #runTime in seconds
    def __init__(self, runTime):
        self.runTime = runTime
        self.startTime = calendar.timegm(time.gmtime())
        
    def get_time(self):
        self.time = self.runTime + (self.startTime - calendar.timegm(time.gmtime()))
        return self.time
    
    def restart(self):
        self.startTime = calendar.timegm(time.gmtime())
        
    #String; must call on get_time
    #Must time.sleep(seconds) to increment
def format_time(badTime):
        minim = badTime // 60
        sec = badTime % 60
        return str(format(minim, '02d')) + ":" + str(format(sec, '02d'))
        
