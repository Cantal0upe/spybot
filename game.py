from random import randint
from collections import namedtuple

Player = namedtuple("Player", ['user', 'role'])
class Game:
    
    
    def __init__(self, roles, names):
        self.spy_index = randint(0, len(names)-1)
        self.players = self.set_roles(roles[1], names)
        self.location = roles[0]

    
    def set_roles(self, roles, names):
        out = []
        for p in names:
            roleind = randint(0, len(roles)-1)
            out.append(Player(user=p, role=roles[roleind]))
            del roles[roleind]
        out[self.spy_index] = out[self.spy_index]._replace(role="spy")
        return out
