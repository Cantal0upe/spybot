import discord
import asyncio
import logging
import Timer
import game
import re
from collections import namedtuple

from random import randint

logging.basicConfig(level=logging.INFO)

client = discord.Client()
players = []
authors = []
authors_dictionary = {}
votes = [0,0,0,0,0,0,0,0,0,0]
game_starting = False
game_running = False
game_struct = []

@client.event
async def on_ready():
    client.connect()
    global game_starting
    game_starting = False
    print("Connected")
    print(client.servers)
    print("Logged in as " + client.user.name)

@client.event
async def on_message(message):
    global players
    global authors
    global authors_dictionary
    global votes
    global game_starting
    global game_running
    global game_struct
    #Checks for command
    
   # await client.send_message(client.get_channel('284872179148128257'), message.channel)
    
    if message.content[0] != '!':
        return
    command = message.content
    #If command is playgame
    if command.startswith("!playgame") and not game_starting:
        await play_game(command)
    #If command is y
        players = []
        game_starting = True
    elif command.startswith("!y") and game_starting:
        if len(players) < 10:
            players.append(message.author.name)
            authors.append(message.author)
            authors_dictionary.setdefault(message.author.name, len(authors)-1)
        else:
            await client.send_message(message.channel, "Sorry, " + message.author.name + ", game is full.")
    elif command.startswith("!start"):
        if len(players) < 3:
            await client.send_message(message.channel, "You need more players.")
        else:
            roles = read_roles()
            game_struct = game.Game(roles[randint(0, len(roles)-1)], players)
            game_running = True
            await run_game()
            game_starting=False
    elif re.fullmatch("![0-9]+", command):        
        votes[int(command[1])] += 1
        total_votes = 0
        for val in votes:
            total_votes += val
        if total_votes == len(players):
            highest = 0
            for index, value in enumerate(votes):
                if votes[index] > votes[highest]:
                    highest = index
            await end_game(highest)
    elif re.fullmatch("![a-zA-z]+[ a-zA-z]*", command):
        if message.author.name == game_struct.players[game_struct.spy_index].name:
            if game_running == False:
               if command[1:len(command)].lower() == game_struct.location.lower():
                    await client.send_message(client.get_channel('284872179148128257'), "The spy guessed the location, the spy wins!")
               else:
                    await client.send_message(client.get_channel('284872179148128257'), "The spy failed to guess to the location, it was " + game_struct.location + ". The players win!")
           
def read_roles():
    global players
    global authors
    global author_dictionary
    global votes
    global game_starting
    global game_running
    global game_struct
    profs = []
    with open("config/roles.txt", "r") as f:
        lines = f.readlines()
    for i in lines:
        lists = i.strip().split(":")
        profs.append((lists[0], lists[1].split(",")))
    return profs

async def play_game(args):
    global players
    global authors
    global author_dictionary
    global votes
    global game_starting
    global game_running
    global game_struct
    await client.send_message(client.get_channel('284872179148128257'), "Say \"!y\" to join the game.")

async def run_game():
    global players
    global authors
    global author_dictionary
    global votes
    global game_starting
    global game_running
    global game_struct
    for player in game_struct.players:
        if player.role != "spy":
            await client.send_message(authors[authors_dictionary.get(player.name)], "Location: " + game_struct.location + " Your Role: " + player.role)
        else:
            await client.send_message(authors[authors_dictionary.get(player.name)], "You are the spy!")
    timer = Timer.Timer(3)
    current_time = timer.get_time()
    prev_time = 0
    msg = await client.send_message(client.get_channel('284872179148128257'), Timer.format_time(current_time))
    while current_time >= 0:
        if current_time != prev_time:
            await client.edit_message(msg, Timer.format_time(current_time))
            prev_time = current_time
        if current_time == 0:
            current_time = -1
        else:
            current_time = timer.get_time()
    if timer.get_time() == 0:
        await vote()

async def vote():
    global players
    global authors
    global author_dictionary
    global votes
    global game_starting
    global game_running
    global game_struct
    await client.send_message(client.get_channel('284872179148128257'), "It's time to vote! Enter ! + the number of the person you think is the spy.")
    for i, p in enumerate(players):
        await client.send_message(client.get_channel('284872179148128257'), str(i)+ ". " + authors[i].name)

async def end_game(chosen):
    global players
    global authors
    global author_dictionary
    global votes
    global game_starting
    global game_running
    global game_struct
    game_running = False
    if game_struct.players[chosen].role == "spy":
        await client.send_message(client.get_channel('284872179148128257'), "The spy was uncovered! Players win!")
    else:
        await client.send_message(client.get_channel('284872179148128257'), "The spy was not uncovered, they should enter their guess for the location as !<location>")

client.run('Mjg0OTMwODg1NDIwMzE4NzIx.C5KyLw.g2mxdn4Cp4sg3w5m4A7WVPrVrd0')
