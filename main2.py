import discord
import asyncio
import timer
import game
import re
import read_file
from collections import namedtuple
from random      import randint
from enum        import Enum

client = discord.Client()



class GameState(Enum):
    INIT = 1
    START = 2
    ADD_PLAYERS = 3
    PLAY = 4
    VOTING = 5
    ENDING = 6

game_struct = []
rtvct = 0
roles = []
players = []
channel = None
state = GameState.INIT
votes = []
rtvbools = [0,0,0,0,0,0,0,0,0,0]

@client.event
async def on_ready():
    client.connect()
    print("logged in as " + client.user.name)
    init()

@client.event
async def on_message(message):
    if not message.content.startswith("!"):
        return
    
    global state
    global rtvct
    global roles
    global players
    global channel
    global votes
    global rtvbools

    if state == GameState.INIT:
        #initialize variables, allow "!channel"
        if message.content.lower == "!channel":
            channel = message.channel

    elif state == GameState.START:
        #allow "!playgame"
        if message.content == "!channel":
            channel = message.channel
        elif message.content == "!playgame":
            await client.send_message(channel, "Send !y to join the game!")
            state = GameState.ADD_PLAYERS
        elif message.content == "!help":
            await client.send_message(channel, "TODO")
        elif message.content.lower() == "!rules":
            await client.send_message(channel, "**The rules of Spyfall**\n\n"
            "At the start of the game, all players are messaged by the SpyfallBot."
            "You will either be notified saying \"You are the spy!\", or you will be sent "
            "a location and a role someone may have in that location."
            "The SpyfallBot will select a player to go first."
            "That player then asks a question to anyone they want to ask."
            "This question should be designed so that the players can determine "
            "who the spy is, without giving information to the spy about what the location is."
            "The person answering should try to convince the asker that they are not the "
            "the spy, while carefully avoiding giving information to the spy.After giving their "
            "answer, that person "
            "then must ask a question to whomever they want.This proceeds until either "
            "the time runs out, the spy makes an attempt to guess the location, or "
            "the majority of players agree to take a vote about who the spy is.\n\n")
            await client.send_message(channel, "\nIf time "
            "runs out, all of the "
            "players, including the spy, take a vote on who they think the spy is. "
            "Whoever has the most votes saying they are the spy must reveal their identity. "
            "If this player is not the spy, the spy wins the game. If this player is the spy"
            ", the spy has one last chance to guess the location. If the spy guesses right, "
            "the spy wins the game, however, if they guess wrongly, all the other players win.\n\n"
            "At any point in the game, the spy may guess the location. If the spy guesses correctly, "
            "the spy wins. If the spy is incorrect, all of the other players win.\n\n"
            "At any point in the game, the players may attempt to initiate a vote."
            "It requires at least half of the players to agree to the vote for it to begin."
            "If a player attempts to initiate a vote, and it is not agreed to by the majority of "
            "players, the game continues as normal. If at least half of the players agree to vote "
            "then every player, including the spy, must submit their vote for who they think the spy is."
            "If the vote is incorrect then the spy wins. If the vote correctly guesses the spy, the non-spy "
            "players all win. The spy does not get to guess the location if the vote is initiated before time "
            "runs out.\n\n")
        elif message.content.lower() == "!locations":
            locls = read_file.get_locations()
            await client.send_message(channel, locls)
    elif state == GameState.ADD_PLAYERS:
        #accept only "!y" or "!Y"
        if message.content.lower() == "!y":
            players.append(message.author)
            votes.append(0)
        elif message.content.lower() == "!start":
            await start() #TODO gives roles to players
    elif state == GameState.PLAY:
        #accept no commands besides "!quit" and "!rtv"
        if message.content.lower() == "!quit":
            state = GameState.INIT
            init() #TODO
        elif message.content.lower() == "!rtv":
            for pl in players:
                if pl.name == message.author.name and not rtvbools[players.index(pl)]:
                    rtvct += 1
                    rtvbools[players.index(pl)] = True       
            if rtvct >= (len(players)+1)/2:
                i = 0
                for name in players:
                    await client.send_message(channel, str(i) + ". " + name.name)
                    i += 1
                await client.send_message(channel, "Please ! + the number of your vote.")
                state = GameState.VOTING
        elif message.content.lower() == "!rtvclear":
            rtvct = 0
    elif state == GameState.VOTING:
        #only accept commands of "![0-9]+"
        if re.fullmatch("![0-9]+", message.content) and 0 <= int(message.content[1:]) <= len(players):
            votes[int(message.content[1:])] += 1
        else:
            await client.send_message(channel, message.author.name + ", that was an invalid vote. Please resubmit.")
        await countvotes() #TODO
    elif state == GameState.ENDING:
        return
    elif message.content.lower().startswith("!guess"):
        if message.author != game_struct.players[game_struct.spy_index]:
            await client.send_message(channel, "A nonspy tried to guess, the spy wins by default.")
            init()
        else:
            if message[7:len(message)].lower() == game_struct.location:
                await client.send_message(channel, "The spy has guess the location, the spy wins!")
            else:
                await client.send_message(channel, "The spy has failed to guess the location, the players win!")
            init()
    elif message.content.lower().startswith("!restart"):
        init()
    else:
        return


def init():
    global roles
    global rtvct
    global players
    global game_struct
    global state
    global votes

    votes = []
    roles = []
    players = []
    rtvct = 0
    game_struct = []

    state = GameState.START

async def start():
    global roles
    global game_struct
    global state
    state = GameState.PLAY
    roles = read_file.get_roles()
    game_struct = game.Game(roles[randint(0, len(roles)-1)], players)
    for i, p in enumerate(game_struct.players):
        if game_struct.players[i].role != "spy":
            await client.send_message(game_struct.players[i].user, "The location is: " + game_struct.location + " Your role is: " + game_struct.players[i].role)
        else:
            await client.send_message(game_struct.players[i].user, "You are the spy!")
    watch = timer.Timer(8*60)
    current_time = watch.get_time()
    prev_time = 0
    msg = await client.send_message(channel, timer.format_time(current_time))
    while current_time >= 0:
        if current_time != prev_time:
            await client.edit_message(msg, timer.format_time(current_time))
            prev_time = current_time
        if current_time == 0:
            current_time = -1
        else:
            current_time = watch.get_time()
    if watch.get_time() == 0:
        state = GameState.VOTING
        await client.send_message(channel, "Time is up, time to vote!")
        await countvotes()
async def countvotes():
    global votes
    global state
    sum = 0
    for i, val in enumerate(votes):
        sum+=val
    if sum < len(players):
        return
    maxV = 0
    for i, val in enumerate(votes):
        if val > votes[maxV]:
            maxV = i
    state = GameState.ENDING
    await choose(maxV)

async def choose(index):
    global channel
    if game_struct.players[index].role == "spy":
        await client.send_message(channel, "The spy was uncovered! The players win!")
        init()
    else:
        await client.send_message(channel, "The spy was not uncovered, they should enter their guess for the location as !<location>.")
        init()
client.run('Mjg0OTMwODg1NDIwMzE4NzIx.C5KyLw.g2mxdn4Cp4sg3w5m4A7WVPrVrd0')
