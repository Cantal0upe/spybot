def get_roles():
        profs = []
        with open("config/roles.txt", "r") as f:
            lines = f.readlines()
        for i in lines:
            lists = i.strip().split(":")
            profs.append((lists[0], lists[1].split(",")))
        return profs


def get_locations():
        locations = []
        with open("config/roles.txt", "r") as f:
            lines = f.readlines()
        for line in lines:
            locations.append(line.split(":")[0])
        return locations 

