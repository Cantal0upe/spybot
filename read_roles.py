
class read_roles:

    def __init__(self):
        self.profs = []
        
    def get_roles(self):
        with open("config/roles.txt", "r") as f:
            lines = f.readlines()
        for i in lines:
            lists = i.strip().split(":")
            self.profs.append((lists[0], lists[1].split(",")))
        return self.profs 
